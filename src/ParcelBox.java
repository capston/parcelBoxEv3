import java.util.HashMap;

/**
 * Created by vmffkxlgnqh1 on 2017. 3. 5..
 */
public class ParcelBox {

    private HashMap<Integer, Integer> boxes = new HashMap<>();
    private int lightBox = 0;
    private int heavyBox = 0;
    private int standard = 1;

    public int inputParcel(int weight, int parcelNumber){
        if( (weight>standard&& heavyBox < 3)||(lightBox >=3 && heavyBox < 3)){
            inputHeavyBox(parcelNumber);
        }
        else if((weight <= standard && lightBox < 3)|| (heavyBox >=3 && lightBox< 3)){
            inputLightBox(parcelNumber);
        }
        else{
            return 7;
        }
        return boxes.get(parcelNumber);
    }
    public void inputLightBox(int parcelNumber){
        for(int i = 0; i < 3; i++){
            if(!boxes.containsValue(i)){
                boxes.put(parcelNumber,i);
                lightBox++;
                break;
            }
        }
    }
    public void inputHeavyBox(int parcelNumber){
        for (int i = 3; i < 6; i++){
            if(!boxes.containsValue(i)){
                boxes.put(parcelNumber,i);
                heavyBox++;
                break;
            }
        }
    }

    public int outputParcel(int parcelNumber){
        int boxNumber = boxes.get(parcelNumber);
        boxes.remove(parcelNumber);
        return boxNumber;
    }

    public boolean isHave(int parcelNumber){
        if(boxes.containsKey(parcelNumber)){
            return true;
        }
        else{
            return false;
        }
    }

    public int getStandard() {
        return standard;
    }
}
