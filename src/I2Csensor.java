import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.I2CSensor;

/**
 * Created by vmffkxlgnqh1 on 2017. 3. 2..
 */
public class I2Csensor {
    static byte[] bufferReadResponse = new byte[8];
    I2CSensor arduino;

    public I2Csensor(int addr){
        arduino = new I2CSensor(SensorPort.S1,addr);
    }
    public int getweight() {
        if (arduino != null) {
            arduino.getData('A', bufferReadResponse, bufferReadResponse.length);
            return Integer.parseInt(bufferReadResponse.toString());
        }
        return 0;
    }

    public void closeArduino(){
        arduino.close();
    }

}
