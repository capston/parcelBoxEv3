import lejos.hardware.lcd.LCD;
import lejos.utility.Delay;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by vmffkxlgnqh1 on 2017. 3. 2..
 */
public class main {

    public static String addr = "192.168.1.107";
    public static int i2c_address;
    public static ParcelBox parcelBox = new ParcelBox();

    public static void main(String[] args){
        SocketServer socketServer = new SocketServer(addr);
        while(true) {
            LCD.clear();
            LCD.drawString("waiting...", 0, 1);

            ParsingJson parsingJson = new ParsingJson();
//            I2Csensor arduino = new I2Csensor(i2c_address);

            String in = socketServer.readJson();
            if (parsingJson.isJson(in)) {
                LCD.clear();
                LCD.drawString("Info received", 0, 1);
                LCD.drawString(in, 0, 2);
                Delay.msDelay(5000);
                parsingJson.Parsing(in);
                if (parsingJson.isInput()) {
//                    int weight = arduino.getweight();
                    int weight = 2;
                    int boxNumber = parcelBox.inputParcel(weight, parsingJson.getParcelNumber());
                    if(boxNumber == 7){
                        LCD.clear();
                        LCD.drawString("Box is full ",0,1);
                        Delay.msDelay(5000);
                    }
                    else{
                        LCD.clear();
                        LCD.drawString("weight is : "+weight, 0, 1);
                        LCD.drawString("parcel is : "+parsingJson.getParcelNumber(), 0, 2);
                        LCD.drawString("number is "+boxNumber+"",0,3);
                        Delay.msDelay(5000);
                    }
                    //sensor's action.
                    socketServer.sendResponse("success");
                    socketServer.closeSocket();
                } else if(parsingJson.isOuput()) {
                    if(parcelBox.isHave(parsingJson.getParcelNumber())){
                        int boxNumber = parcelBox.outputParcel(parsingJson.getParcelNumber());
                        //sensor's action.
                    }
                    else{
                        LCD.clear();
                        LCD.drawString("No found Parcel", 0, 1);
                        Delay.msDelay(5000);
                    }

                } else{
                    LCD.clear();
                    LCD.drawString("Wrong method", 0, 1);
                    Delay.msDelay(5000);
                }
            } else {
                LCD.clear();
                LCD.drawString("wrong request", 0, 1);
                Delay.msDelay(5000);
            }
        }
    }
}
