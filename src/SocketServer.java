import lejos.hardware.lcd.LCD;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

/**
 * Created by vmffkxlgnqh1 on 2017. 3. 2..
 */
public class SocketServer {

    private static final int PORT = 3000;
    private ServerSocket server;
    public String url;
    private Socket client;

    public SocketServer(String url){
        this.url = url;
        try {
            server = new ServerSocket(PORT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public ServerSocket getServer(){
        return server;
    }
    public String readJson(){
        client = null;
        String in = null;
        try {
            client = server.accept();
            LCD.drawString("connect",0,2);
            BufferedReader buffer = new BufferedReader(new InputStreamReader(client.getInputStream()));
            in = buffer.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return in;
    }
    public void closeSocket(){
        try {
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void sendResponse(String response) {
        PrintWriter outgoing = null;
        try {
            outgoing = new PrintWriter(client.getOutputStream());
            outgoing.print(response);
            outgoing.flush();
            outgoing.close();
            LCD.clear();
            LCD.drawString("send : "+response, 0, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
