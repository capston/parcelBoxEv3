import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import lejos.hardware.lcd.LCD;


/**
 * Created by vmffkxlgnqh1 on 2017. 3. 5..
 */
public class ParsingJson {

    private Gson gson;
    private JsonObject jsonObject;
    private String method;
    private int parcelNumber;

    public ParsingJson(){
        gson = new GsonBuilder().create();
    }
    //validate json.
    //if the string is json, parsing that.
    public boolean isJson(String json){
        try {
            gson.fromJson(json, JsonObject.class);
            return true;
        }catch(Exception e){
            return false;
        }
    }
    public void Parsing(String json){
        jsonObject = gson.fromJson(json,JsonObject.class);
        method = jsonObject.get("method").getAsString();
        parcelNumber = jsonObject.get("parcelNumber").getAsInt();
    }
    public boolean isInput(){
        if(method.matches("input")){
            LCD.clear();
            LCD.drawString("in input",0,1);
            return true;
        }
        return false;
    }
    public boolean isOuput(){
        if(method.matches("Output")){
            LCD.clear();
            LCD.drawString("in input",0,1);
            return true;
        }
        return false;
    }
    public int getParcelNumber(){
        return parcelNumber;
    }


}
